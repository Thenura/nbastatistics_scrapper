# By: Thenura Jayasinghe
# Date: December 25, 2019
# Description: Scrapes player statistics from website and exports to CSV

from bs4 import BeautifulSoup
import requests
import csv

# Initialize CSV File Name
f = open ('PlayerStatistics.csv', 'w')
writer = csv.writer(f)

page = requests.get("https://basketball.realgm.com/nba/stats")
soup = BeautifulSoup(page.content, 'html.parser')
rows = soup.find("table", {"class": "tablesaw compact"})

# Retrieve all cells in the row
# Note that body data separate from the header data
table_data_body = rows.tbody.find_all("tr")
table_data_headers = rows.thead.find_all("tr")

# Iterates through the headers in the table
for tr in table_data_headers:
    cols = tr.find_all('a')

    # Added the '#' sign to show the player ranking symbol
    # Unable to retrieve through BeautifulSoup function - manually added
    headers = "#"
    for td in cols:
        headers = headers + " " + td.find(text=True)

    header_rows = headers.split(" ")
    writer.writerow(header_rows)


# Iterates through the rows in the table
for tr in table_data_body:
    cols = tr.find_all('td')

    #Iterates through the cells of each row
    player = ""
    for td in cols:
        # Added deliminator using '|' because when using spacing, creates
        # extra columns for players with 'Jr.'
        player = player + "|" + str(td.find(text=True))

    player_row = player[1:].split("|")
    writer.writerow(player_row)

print('Data Retrieved')
print('Check CSV File')
